package com.example.stores

import android.app.Application
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.stores.common.database.StoreDatabase

//Para instanciar storeApplication se tiene que declarar en el manifest para poder ser utilizado
class StoreApplication:Application() {

    companion object{
        lateinit var database: StoreDatabase
    }
    override fun onCreate() {
        super.onCreate()

        //En esta constante vamos a declarar que cambios se realizo en la bd

        val MIGRATION_1_2 = object : Migration(1,2){
            override fun migrate(database: SupportSQLiteDatabase) {
                //setaremos que tabla fue modificada asi como la columna,
                //que pasará con los registros que ya existen y no tienen esa columna
                //agregamos el tipo TEXT y le decimos que es un (NOT NULL DEFAULT '') OSEA UN VALOR VACIO
                database.execSQL("ALTER TABLE StoreEntity ADD COLUMN photoUrl TEXT NOT NULL DEFAULT ''")
            }
        }

        database = Room.databaseBuilder(this,
            StoreDatabase::class.java,
            "StoreDatabase")
            .addMigrations(MIGRATION_1_2) //agregamos el cambio que se realizó
            .build()
    }
}