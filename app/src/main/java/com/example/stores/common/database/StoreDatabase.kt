package com.example.stores.common.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.stores.common.entities.StoreEntity

//Cada vez que se hace un cambio en la bd se modifica la version

@Database(entities = arrayOf(StoreEntity::class), version = 2)
abstract class StoreDatabase:RoomDatabase() {
    abstract fun storeDao(): StoreDao
}