package com.example.stores.common.utils

import com.example.stores.common.entities.StoreEntity

//En esta interface se agregar metodos que vamos a implementar en las clases como buenas practicas
interface MainAux {

    fun hideFab(isVisible:Boolean = false)
    fun addStore(storeEntity: StoreEntity)
    fun updateStore(storeEntity: StoreEntity)
}