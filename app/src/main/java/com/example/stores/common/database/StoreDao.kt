package com.example.stores.common.database

import androidx.room.*
import com.example.stores.common.entities.StoreEntity

@Dao
interface StoreDao {

    @Query("SELECT * FROM StoreEntity")
    fun getAllStore() : MutableList<StoreEntity>

    //realizamos una consulta en base al id de la tabla para que nos devuelva todos los datos de la
    //tabla getStoreById(id:Long): StoreEntity <-
    @Query("SELECT * FROM StoreEntity WHERE id = :id")
    fun getStoreById(id:Long): StoreEntity

    //En addStore tenemos que decir que nos devuelva un valor
    //de tipo Long (el mismo del id de la tienda) para poder
    // actulizar sus datos
    @Insert
    fun addStore(storeEntity: StoreEntity): Long

    @Update
    fun updateStore(storeEntity: StoreEntity)

    @Delete
    fun deleteStore(storeEntity: StoreEntity)
}