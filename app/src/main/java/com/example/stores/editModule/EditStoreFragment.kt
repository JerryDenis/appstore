package com.example.stores.editModule

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.stores.R
import com.example.stores.StoreApplication
import com.example.stores.common.entities.StoreEntity
import com.example.stores.databinding.FragmentEditStoreBinding
import com.example.stores.editModule.viewModel.EditStoreViewModel
import com.example.stores.mainModule.MainActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class EditStoreFragment : Fragment() {

    private lateinit var mBinding: FragmentEditStoreBinding
    //MVVM
    private lateinit var mEditStoreViewModel: EditStoreViewModel
    private var mActivity: MainActivity? = null

    private var mIsEditMode:Boolean = false
    private lateinit var mStoreEntity: StoreEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mEditStoreViewModel = ViewModelProvider(requireActivity()).get(EditStoreViewModel::class.java)
    }
    //aqui se vincula la vista e inicializar la vista
    override fun onCreateView(inflater: LayoutInflater, container:
    ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentEditStoreBinding.inflate(inflater,container,false)

        return mBinding.root
    }

    //La vista cuando ha sido creado completamente
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //En esta arreglo agregamos un oyente para setear un link url y previsualizar la img

       /* mBinding.editPhotoUrl.addTextChangedListener {
            Glide.with(this)
                .load(mBinding.editPhotoUrl.text.toString())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(mBinding.imgPhoto)
        }*/
        //MVVM
        setupViewModel()

        setupTextFields()
    }

    private fun setupViewModel() {
        mEditStoreViewModel.getStoreSelected().observe(viewLifecycleOwner) {
            mStoreEntity = it
            if (it.id != 0L) {
                mIsEditMode = true
                setUiStore(it)
            } else {
                mIsEditMode = false
            }

            setupActionBar()
        }

        mEditStoreViewModel.getResult().observe(viewLifecycleOwner) { result ->
            //Ocultar teclado
            //nota: ocultar primero el teclado sino el snackbar de mostrará arriba de ello
            hideKeyboard()

            when(result){
                is Long -> {
                    //llamamos a la actividad main y su metodo para agregar un item en el adapter gridlayoutview
                    //Toast message success
                    mStoreEntity.id = result
                    mEditStoreViewModel.setStoreSelect(mStoreEntity)

                    Toast.makeText(
                        mActivity,
                        getString(R.string.edit_store_message_save_success),
                        Toast.LENGTH_LONG
                    ).show()
                    mActivity?.onBackPressed()
                }
                is StoreEntity ->{
                    mEditStoreViewModel.setStoreSelect(mStoreEntity)
                    Snackbar.make(
                        mBinding.root,
                        getString(R.string.edit_store_message_update_success),
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun setupActionBar() {
        mActivity = activity as? MainActivity
        //aqui agregamos la flecha de back en el actionbar <-
        mActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //aqui agregamos un title al actionbar
        mActivity?.supportActionBar?.title = if (mIsEditMode) getString(R.string.edit_store_title_edit)
                                             else getString(R.string.edit_store_title_add)

        //Se obtiene acceso al menu
        setHasOptionsMenu(true)
    }

    private fun setupTextFields() {
        with(mBinding){
            editPhotoUrl.addTextChangedListener {
                validateFields(tilPhotoUrl)
                loadImage(it.toString())}
            editPhone.addTextChangedListener { validateFields(tilPhone) }
            editName.addTextChangedListener { validateFields(tilName) }
        }
    }

    private fun loadImage(url:String) {
        Glide.with(this)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(mBinding.imgPhoto)
    }

    private fun setUiStore(storeEntity: StoreEntity) {
        //1ra forma
        with(mBinding){
            //editName.text = Editable.Factory.getInstance().newEditable(storeEntity.name) <- #1 forma
            //editName.setText(storeEntity.name) <- #2 forma
            editName.text = storeEntity.name.editable()
            editPhone.text = storeEntity.telefono.editable() //#3 forma
            editWebsite.text = storeEntity.website.editable()
            editPhotoUrl.text = storeEntity.photoUrl.editable()
            //ya no se hace el llamado de Glide porque dentro de OnViewCreated() se hace referencia
           // a addTextChangedListener con Glide
           /* Glide.with(mActivity!!)
                .load(storeEntity.photoUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(imgPhoto)*/
        }
    }
    //Extensiones de kotlin son usadas para las buenas practicas # 3 forma
    private fun String.editable(): Editable = Editable.Factory.getInstance().newEditable(this)

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        //inflamos el menu para que sea visible
        inflater.inflate(R.menu.menu_save,menu)
        super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            //home es el id de setdisplayhome
            android.R.id.home ->{
                //acción de retorno de vista
                mActivity?.onBackPressed()
                true
            }
            R.id.action_save ->{
                //mStoreEntity tiene que validarse como buenas practicas
                if (validateFields(mBinding.tilPhotoUrl,mBinding.tilPhone,mBinding.tilName)) {
                    with(mStoreEntity) {
                        name = mBinding.editName.text.toString().trim()
                        telefono = mBinding.editPhone.text.toString().trim()
                        website = mBinding.editWebsite.text.toString().trim()
                        photoUrl = mBinding.editPhotoUrl.text.toString().trim()
                    }

                    if (mIsEditMode) mEditStoreViewModel.updateStore(mStoreEntity)
                    else mEditStoreViewModel.saveStore(mStoreEntity)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)

        }
        //return super.onOptionsItemSelected(item)
    }
    //validamos los campos de texto para que no sean null o vacio los requeridos #1
    private fun validateFields(vararg textFields: TextInputLayout):Boolean{
        var isValid = true

        for(textField in textFields){
            if (textField.editText?.text.toString().trim().isEmpty()){
                textField.error = getString(R.string.helper_required)
                //textField.editText?.requestFocus()
                isValid = false
            }else{
                textField.error = null
            }
        }
        if (!isValid) Snackbar.make(mBinding.root,
            getString(R.string.edit_store_message_valid),
            Snackbar.LENGTH_SHORT).show()

        return isValid
    }
    //validamos los campos de texto para que no sean null o vacio los requeridos #2
   /* private fun validateFields(): Boolean {
        var isValid = true
        if (mBinding.editPhotoUrl.text.toString().trim().isEmpty()){
            mBinding.tilPhotoUrl.error = getString(R.string.helper_required)
            mBinding.tilPhotoUrl.requestFocus()
            isValid = false
        }
        if (mBinding.editPhone.text.toString().trim().isEmpty()){
            mBinding.tilPhone.error = getString(R.string.helper_required)
            mBinding.tilPhone.requestFocus()
            isValid = false
        }
        if (mBinding.editName.text.toString().trim().isEmpty()){
            mBinding.tilName.error = getString(R.string.helper_required)
            mBinding.tilName.requestFocus()
            isValid = false
        }
        return isValid
    }*/

    //metodo para ocultar teclado
    private fun hideKeyboard(){
        mActivity?.currentFocus?.let { view ->
            val imm = mActivity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
        /*
        val imm = mActivity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)*/
    }

    override fun onDestroyView() {
        hideKeyboard()
        super.onDestroyView()
    }

    override fun onDestroy() {
        mActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        mActivity?.supportActionBar?.title = getString(R.string.app_name)
        mEditStoreViewModel.setResult(Any())
        setHasOptionsMenu(false)
        mEditStoreViewModel.setShowFab(true)
        super.onDestroy()
    }

}