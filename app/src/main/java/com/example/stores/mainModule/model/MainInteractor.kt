package com.example.stores.mainModule.model

import com.example.stores.StoreApplication
import com.example.stores.common.entities.StoreEntity
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainInteractor {

   /* Prueba modo de ilustracion
   interface StoresCallBack{
        fun getStoresCallBack(stores: MutableList<StoreEntity>)
    }

    fun getStoresCallBack(callBack: StoresCallBack){
        doAsync {
            val storesList = StoreApplication.database.storeDao().getAllStore()
            uiThread {
                callBack.getStoresCallBack(storesList)
            }
        }
    }*/

    fun getStores(callBack: (MutableList<StoreEntity>) -> Unit ){
        doAsync {
            val storesList = StoreApplication.database.storeDao().getAllStore()
            uiThread {
                callBack(storesList)
            }
        }
    }

    fun deleteStore(storeEntity:StoreEntity, callBack: (StoreEntity) -> Unit){
        doAsync {
            StoreApplication.database.storeDao().deleteStore(storeEntity)
            uiThread {
                callBack(storeEntity)
            }
        }

    }

    fun updateStore(storeEntity: StoreEntity,callBack: (StoreEntity) -> Unit){
        doAsync {
            StoreApplication.database.storeDao().updateStore(storeEntity)
            uiThread {
                //mAdapter.update(storeEntity)
                callBack(storeEntity)
            }
        }
    }
}