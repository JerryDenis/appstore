package com.example.stores.mainModule

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.stores.*
import com.example.stores.common.entities.StoreEntity
import com.example.stores.common.utils.MainAux
import com.example.stores.databinding.ActivityMainBinding
import com.example.stores.editModule.EditStoreFragment
import com.example.stores.editModule.viewModel.EditStoreViewModel
import com.example.stores.mainModule.adapter.OnClicklistener
import com.example.stores.mainModule.adapter.StoreAdapter
import com.example.stores.mainModule.viewModel.MainViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity(), OnClicklistener {

    private lateinit var mbinding: ActivityMainBinding

    private lateinit var mAdapter: StoreAdapter
    private lateinit var mGridlayout:GridLayoutManager

    //MVVM
    private lateinit var mMainViewModel:MainViewModel
    private lateinit var mEditStoreViewModel: EditStoreViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mbinding.root)

        /*mbinding.btnSave.setOnClickListener {
            val store = StoreEntity(name = mbinding.etName.text.toString().trim())
            Thread{ StoreApplication.database.storeDao().addStore(store)
            }.start()
            mAdapter.add(store)

        }*/
        setupViewModel()
        setupRecyclerView()
        mbinding.fab.setOnClickListener { launchEditFragment() }

    }

    private fun setupViewModel() {
        mMainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mMainViewModel.getStore().observe(this,{stores ->
            mAdapter.setStores(stores)

        })
        mEditStoreViewModel = ViewModelProvider(this).get(EditStoreViewModel::class.java)
        mEditStoreViewModel.getShowFab().observe(this,{ isVisible ->
            if (isVisible) mbinding.fab.show() else mbinding.fab.hide()

        })
        mEditStoreViewModel.getStoreSelected().observe(this,{ storeEntity ->
            mAdapter.add(storeEntity)

        })
      //
    }

    private fun launchEditFragment(storeEntity: StoreEntity = StoreEntity()) {
        mEditStoreViewModel.setShowFab(false)
        mEditStoreViewModel.setStoreSelect(storeEntity)
        val fragment = EditStoreFragment()
        //enviamos argumentos al fragmento
       // if (args != null) fragment.arguments = args
        //
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        fragmentTransaction.add(R.id.containerMain,fragment)
        //Para retroceder la acción o la vista
        fragmentTransaction.addToBackStack(null)

        fragmentTransaction.commit()
       // mbinding.fab.hide()

    }

    private fun setupRecyclerView() {
        mAdapter = StoreAdapter(mutableListOf(),this)
        mGridlayout = GridLayoutManager(this,resources.getInteger(R.integer.main_columns))
        //getStores()

        mbinding.recyclerHome.apply {
            setHasFixedSize(true)
            layoutManager = mGridlayout
            adapter = mAdapter

        }

    }

    /*private fun getStores(){
        doAsync {
            val stores = StoreApplication.database.storeDao().getAllStore()
            uiThread {
                 mAdapter.setStores(stores)
            }
        }
    }*/

    override fun OnClick(storeEntity: StoreEntity) {
        //A este args de tipo bundle le pasaremos los argumentos
        //val args = Bundle()
        // pasaremos el id de tipo long - a putlong tenemos que pasar un key y valor
        //args.putLong(getString(R.string.arg_id), storeId)
        launchEditFragment(storeEntity)

    }
    //pasaremos storeentity de tipo StoreEntity cuando tengamos una bd mucho mas grande con mas items
    /*override fun OnClick(storeEntity: StoreEntity) {
        //A este args de tipo bundle le pasaremos los argumentos
        val args = Bundle()
        // pasaremos el id de tipo long
        args.putLong()

    }*/

    override fun OnFavoriteStore(storeEntity: StoreEntity) {
        mMainViewModel.updateStore(storeEntity)
        /*doAsync {
            StoreApplication.database.storeDao().updateStore(storeEntity)
            uiThread {
                //mAdapter.update(storeEntity)
                updateStore(storeEntity)
            }
        }*/
    }

    override fun OnDeleteStore(storeEntity: StoreEntity) {
        //val items = arrayOf("Eliminar","Llamar","Ir al sitio Web")
        val items = resources.getStringArray(R.array.array_options_item)

        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.dialog_options_title)
            .setItems(items) { _, i ->
                when (i) {
                    0 -> confirmDelete(storeEntity)

                    1 -> dial(storeEntity.telefono)

                    2 -> goToWebSite(storeEntity.website)
                }
            }
            .show()

    }
    //Aqui se hace el setup para poder utilizar una actividad fuera del proyecto - llamar a telefono
    //para que esta funcion se llegue a utilizar de manera comercial android nos pedira permisos
    private fun dial(phone:String){
        val callIntent = Intent().apply {
            action = Intent.ACTION_DIAL
            //se tiene que colocar tel: dentro de Uri.parse para que el sdk de android lo reconozca
            data = Uri.parse("tel:$phone")
        }
        startIntent(callIntent)
    }

    private fun goToWebSite(webSite: String){
        if(webSite.isEmpty()){
            Toast.makeText(this, R.string.main_error_no_website,Toast.LENGTH_LONG).show()
        }else{
            val webSiteIntent = Intent().apply {
                action = Intent.ACTION_VIEW
                data = Uri.parse(webSite)
            }
            startIntent(webSiteIntent)
        }
    }

    private fun startIntent(intent: Intent){
        //Esta validación funciona para el api 24 hacia abajo sin permisos api 30 con permisos en el
        //manifest
        if (intent.resolveActivity(packageManager) != null){
            startActivity(intent)
        }else{
            Toast.makeText(this, R.string.main_error_no_resolve,Toast.LENGTH_LONG).show()
        }
    }

    private fun confirmDelete(storeEntity: StoreEntity){
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.dialog_delete_title)
            .setPositiveButton(R.string.dialog_delete_confirm) { _, _ ->
                mMainViewModel.deleteStore(storeEntity)
               /* doAsync {
                    StoreApplication.database.storeDao().deleteStore(storeEntity)
                    uiThread {
                        mAdapter.delete(storeEntity)
                    }
                }*/
            }
            .setNegativeButton(R.string.dialog_delete_cancel, null)
            .show()
    }


}