package com.example.stores.mainModule.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.stores.StoreApplication
import com.example.stores.common.entities.StoreEntity
import com.example.stores.mainModule.model.MainInteractor
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.concurrent.LinkedBlockingDeque
import kotlin.concurrent.thread

class MainViewModel:ViewModel() {

    private var storeList: MutableList<StoreEntity>
    private var interactor:MainInteractor

    init {
        storeList = mutableListOf()
        interactor = MainInteractor()
        //loadStores()

    }
    //lazy nos ayuda a inicializar una constante y la segunda vez se reutilza en valor obtenido anteriormente
    private val stores : MutableLiveData<List<StoreEntity>> by lazy {
        MutableLiveData<List<StoreEntity>>().also {//descomentar con corrutines
            loadStores()
        }
    }

    fun getStore(): LiveData<List<StoreEntity>>{
        return stores//.also { loadStores() }
    }

    private fun loadStores(){
        //paso 1 -> Esto es lo mismo que el paso 2
        /*interactor.getStoresCallBack(object : MainInteractor.StoresCallBack{
            override fun getStoresCallBack(stores: MutableList<StoreEntity>) {
                this@MainViewModel.stores.value = stores
            }
        })*/
        //paso 2
        interactor.getStores {
            stores.value = it
            storeList = it
        }
        /*----------*/
        /* Anko
        doAsync {
            val storesList = StoreApplication.database.storeDao().getAllStore()
            uiThread {
                 store.value = storesList
            }
        }*/

        /* este arreglo de codigo funcionaría sin anko
        val queue = LinkedBlockingDeque<MutableList<StoreEntity>>()
        thread {
            val storesList = StoreApplication.database.storeDao().getAllStore()
            queue.add(storesList)
        }.start()
        store.value = queue.take()*/
    }

    fun deleteStore(storeEntity: StoreEntity){
        interactor.deleteStore(storeEntity, {
            val index = storeList.indexOf(storeEntity)
            if (index != -1){
                storeList.removeAt(index)
                stores.value = storeList
            }
        })
    }
    fun updateStore(storeEntity: StoreEntity){
        storeEntity.isfavorite = !storeEntity.isfavorite
        interactor.updateStore(storeEntity,{
            val index = storeList.indexOf(storeEntity)
            if (index != -1){
                storeList.set(index,storeEntity)
            }
        })
    }

}