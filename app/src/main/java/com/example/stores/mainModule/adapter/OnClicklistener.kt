package com.example.stores.mainModule.adapter

import com.example.stores.common.entities.StoreEntity

interface OnClicklistener {
    fun OnClick(storeEntity: StoreEntity)//metodo para agregar items
    //fun OnClick(storeEntity: StoreEntity) //aplicaremos esto cuando tengamos mucho mas items
    fun OnFavoriteStore(storeEntity: StoreEntity) //metodo para seleccionar items favoritos
    fun OnDeleteStore(storeEntity: StoreEntity) //metodo para eliminar items
}