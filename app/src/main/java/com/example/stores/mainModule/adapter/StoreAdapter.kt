package com.example.stores.mainModule.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.stores.R
import com.example.stores.common.entities.StoreEntity
import com.example.stores.databinding.ItemStoreBinding

class StoreAdapter (private var stores: MutableList<StoreEntity>, private var listener: OnClicklistener):
    RecyclerView.Adapter<StoreAdapter.ViewHolder>(){


    private lateinit var mContext: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context

        val view = LayoutInflater.from(mContext).inflate(R.layout.item_store,parent,false)

        return ViewHolder(view)
    }
    //Aqui se declara t0d0 lo que será visible para el usuario
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //
        val store = stores.get(position)

        with(holder){
            setListener(store)
            binding.tvName.text = store.name
            binding.cbFavorite.isChecked = store.isfavorite

            //con glide cargamos la imagen en el gridlayoutview de la tienda
            Glide.with(mContext)
                .load(store.photoUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.imagePhoto)

        }

    }

    //Verifica el tamaño de la lista store
    override fun getItemCount(): Int = stores.size

    //setear items a una lista
    fun setStores(stores: List<StoreEntity>) {
        this.stores = stores as MutableList<StoreEntity>
        notifyDataSetChanged()
    }

    fun add(storeEntity: StoreEntity) {
        //antes de agregar un item vamos a preguntar si ya existe o no
        if (storeEntity.id != 0L) {
            if (!stores.contains(storeEntity)) {
                stores.add(storeEntity)
                //notifyItemInsert es para pasar un posision al item nuevo
                //stores.size-1 <- con esto le decimos que pase ser agregado al ultimo de la vista
                notifyItemInserted(stores.size-1)
                //notifyDataSetChanged() <- para actualizar despues de agregar un item
            }else{
                update(storeEntity)
            }
        }
    }

   private fun update(storeEntity: StoreEntity) {
        val index = stores.indexOf(storeEntity)
        if (index != -1){
            stores.set(index,storeEntity)
            notifyItemChanged(index)
        }

    }

   /* fun delete(storeEntity: StoreEntity) {
        val index = stores.indexOf(storeEntity)
        if (index != -1){
            stores.removeAt(index)
            notifyItemRemoved(index)
        }
    }*/

    inner class ViewHolder(view: View):RecyclerView.ViewHolder(view){

        val binding = ItemStoreBinding.bind(view)

        fun setListener(storeEntity: StoreEntity){

            //binding.root puede ser mejorado con el metodo with()

            //listener para agregar items
            //estamos modificando porque el metodo onClick de la interface requiere el id de tipo
            // Long y el item id de storeEntity es de tipo Long
            with(binding) {
                root.setOnClickListener {
                    listener.OnClick(storeEntity)

                }
                //listener para agregar favoritos al items = update
                cbFavorite.setOnClickListener {
                    listener.OnFavoriteStore(storeEntity)
                }
                //En SetOnLongClickListener el tipo boolean (true or false) debe ir en otra linea
                //listener para eliminar items
                root.setOnLongClickListener {
                    listener.OnDeleteStore(storeEntity)
                    true
                }
            }
        }

    }


}